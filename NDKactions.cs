﻿
using System.Collections;

using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

using System.IO;
using AndroidNative;

public class NDKactions {

	static class Coms {
		public const   string ndk_build = "ndk-build";

	}
	static  class Parms
	{
		public static Parm APPLICATION_MK = new Parm ("NDK_APPLICATION_MK=",false);
		public static Parm PROJECT_PATH = new Parm ("NDK_PROJECT_PATH=",false);
		public static Parm clean = new Parm ("clean",false);
		public static Parm ANDROID_MK = new Parm ("APP_BUILD_SCRIPT=",false);
		public static Parm SRC_FILES = new Parm ("LOCAL_SRC_FILES=",false);

	}

	public NDKactions (){


	}


	public static void compile ( string src){
		string dir = src;

		string file = Path.Combine (Lib.NDKpath, Coms.ndk_build);

		string args = Ndk_build.arguments(new string[] { 
			Parms.ANDROID_MK.value(Path.Combine(src , "Android.mk"))
			,Parms.PROJECT_PATH.value(src)
			,Parms.APPLICATION_MK.value(Path.Combine(src,"Application.mk"))
			,Parms.SRC_FILES.value(Path.Combine(src , "NativeCode.c"))

		});



		
			
		OS.run (file, args,dir);


	}


}
