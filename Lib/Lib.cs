﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;

namespace AndroidNative
{	
	[System.Serializable]
	public class Lib  {
			

			
		static public string NDKpath = @"/Users/Shared/Library/android-sdk-macosx/NDK/android-ndk-r10d/" ;
		static public string SDKpath = @"/Users/Shared/Library/android-sdk-macosx/";
		static public string JDKpath = @"/Library/Java/JavaVirtualMachines/jdk1.7.0_75.jdk/Contents/Home/bin/";
		


		public static string addSpaces (string [] parms){
			string result = "";
			foreach (string parm in parms) {
				result += " " + parm;
			}
			
			return result;
		}


		
	}


	public class Parm {
		public readonly string NAME;
		public readonly bool ADD_SPACE;
		public readonly bool CROSS_PLATFORM;
		public readonly string BREAK;
		public readonly string WIN_BREAK;
		public readonly string MAC_BREAK;

		public Parm(string name, bool addSpace, string BREAK){
			this.NAME = name;
			this.ADD_SPACE = addSpace;
			this.BREAK = BREAK;
			this.WIN_BREAK = null;
			this.MAC_BREAK = null;
			this.CROSS_PLATFORM = true;
		}



		public Parm(string name, bool addSpace){
			this.NAME = name;
			this.ADD_SPACE = addSpace;
			this.BREAK = null;
			this.WIN_BREAK = null;
			this.MAC_BREAK = null;
			this.CROSS_PLATFORM = true;
		}

		public Parm(string name, bool addSpace, string winBreak,string macBreak){
			this.NAME = name;
			this.ADD_SPACE = addSpace;


			//ought to be changed later, croos platform
			this.BREAK = null;
			this.WIN_BREAK = winBreak;
			this.MAC_BREAK = macBreak;
			this.CROSS_PLATFORM = false;
		}


		public string value (string [] values){
			string result = "";
				if(ADD_SPACE)  result = " ";

			if (BREAK == null)
				throw new System.ArgumentException (this.NAME + " doesn't take more than one argument");

			result += values [0];




			for (int i =1; i<values.Length; i++)
				result += this.BREAK +  values [i];

			return this.NAME + result;
		}

		public string value (string  value){
			string result = "";
			if(ADD_SPACE)  result = " ";
			result += value;
			return this.NAME + result;
				
		}


	}



	public static class Javac {



		public static string arguments (string [] parms, string [] classes){
			return Lib.addSpaces (parms) + " " + Lib.addSpaces (classes);
		}

	}

	public static class Ndk_build {

		public static string arguments (string [] parms){
			return Lib.addSpaces(parms);
		}

	}

	[System.Serializable]
	public static class Jar {

		public const  string jar = "jar";


		public static string arguments (string options, string  jarName, string dir){


			//if MAC
			Del (dir);

			return options + " " +  jarName + " " + ".";

		}

		private static void Del(string sDir)
		{
					


			try
			{

				foreach (string f in Directory.GetFiles(sDir))
				{
					string exten = Path.GetExtension (f);
					Debug.Log (exten);
					if(exten.Equals(".DS_Store"))
						File.Delete(f);
				}

				foreach (string d in Directory.GetDirectories(sDir))
				{


					Del(d);
				}


			}
			catch (System.Exception excpt)
			{
				Debug.LogError(excpt.Message);
			}
			

		}

	}

	public static class MK {



		public  class IncVars {
			public const string CLEAR_VARS = "CLEAR_VARS";
			public const string BUILD_SHARED_LIBRARY = "BUILD_SHARED_LIBRARY";
			public const string BUILD_STATIC_LIBRARY = "BUILD_STATIC_LIBRARY";
			public const string PREBUILT_SHARED_LIBRARY = "PREBUILT_SHARED_LIBRARY";
			public const string PREBUILT_STATIC_LIBRARY = "PREBUILT_STATIC_LIBRARY";


		}

		public  class Var {
			public readonly string NAME;

			public Var (string name){
				this.NAME = name;
			}

			//equal,assign
			public  string eq(string val){
				return this.NAME + " := " + val;
			}

			//equal,assign
			public string eq(string[] vals){
				string temp = "";
				
				foreach (string val in vals)
					temp += val + " ";
				
				return this.NAME + " := " + temp;
			}
		}

		public static class Vars {
			public static readonly Var LOCAL_MODULE = new Var("LOCAL_MODULE");
			public static  readonly Var LOCAL_SRC_FILES = new Var("LOCAL_SRC_FILES");
			public static  readonly Var LOCAL_PATH = new Var ("LOCAL_PATH");
			public static  readonly Var PREBUILT_STATIC_LIBRARY = new Var ("PREBUILT_STATIC_LIBRARY");
			public static  readonly Var TARGET_ARCH = new Var ("TARGET_ARCH");
			public static  readonly Var TARGET_ARCH_ABI = new Var ("TARGET_ARCH_ABI");
			public static  readonly Var TARGET_ABI = new Var ("TARGET_ABI");
			public static  readonly Var NDK_MODULE_PATH = new Var ("NDK_MODULE_PATH"); // I'm not sure

			public static  readonly Var LOCAL_MODULE_FILENAME = new Var ("LOCAL_MODULE_FILENAME");

			public static readonly Var LOCAL_CPP_FEATURES = new Var ("LOCAL_CPP_FEATURES");

			public static  readonly Var LOCAL_C_INCLUDES = new Var ("LOCAL_C_INCLUDES");
			public static  readonly Var LOCAL_CFLAGS = new Var ("LOCAL_CFLAGS");


			public static  readonly Var LOCAL_CPPFLAGS = new Var ("LOCAL_CPPFLAGS");
			public static  readonly Var LOCAL_STATIC_LIBRARIES = new Var ("LOCAL_STATIC_LIBRARIES");
			public static  readonly Var LOCAL_SHARED_LIBRARIES = new Var ("LOCAL_SHARED_LIBRARIES");
			public static  readonly Var LOCAL_WHOLE_STATIC_LIBRARIES = new Var ("LOCAL_WHOLE_STATIC_LIBRARIES");
			public static readonly Var LOCAL_LDLIBS = new Var ("LOCAL_LDLIBS");
			public static  readonly Var LOCAL_LDFLAGS = new Var ("LOCAL_LDFLAGS");
			public static  readonly Var LOCAL_ALLOW_UNDEFINED_SYMBOLS = new Var ("LOCAL_ALLOW_UNDEFINED_SYMBOLS");
			public static  readonly Var LOCAL_ARM_MODE = new Var ("LOCAL_ARM_MODE");
			public static  readonly Var LOCAL_ARM_NEON = new Var ("LOCAL_ARM_NEON");
			public static  readonly Var LOCAL_DISABLE_NO_EXECUTE = new Var ("LOCAL_DISABLE_NO_EXECUTE");
			public static  readonly Var LOCAL_DISABLE_RELRO = new Var ("LOCAL_DISABLE_RELRO");
			public static  readonly Var LOCAL_DISABLE_FORMAT_STRING_CHECKS = new Var ("LOCAL_DISABLE_FORMAT_STRING_CHECKS");
			public static  readonly Var LOCAL_EXPORT_CFLAGS = new Var ("LOCAL_EXPORT_CFLAGS");
			public static  readonly Var LOCAL_EXPORT_CPPFLAGS = new Var ("LOCAL_EXPORT_CPPFLAGS");
			public static  readonly Var LOCAL_EXPORT_C_INCLUDES = new Var ("LOCAL_EXPORT_C_INCLUDES");
			public static  readonly Var LOCAL_EXPORT_LDFLAGS = new Var ("LOCAL_EXPORT_LDFLAGS");
			public static  readonly Var LOCAL_EXPORT_LDLIBS = new Var ("LOCAL_EXPORT_LDLIBS");
			public static  readonly Var LOCAL_SHORT_COMMANDS = new Var ("LOCAL_SHORT_COMMANDS");
			public static  readonly Var LOCAL_THIN_ARCHIVE = new Var ("LOCAL_THIN_ARCHIVE");
			public static  readonly Var LOCAL_FILTER_ASM = new Var ("LOCAL_FILTER_ASM");



		}


		public struct Macros { 
			public const string my_dir = "call my-dir";
			public const string all_subdir_makefiles = "call all-subdir-makefiles";
			public const string this_makefile = "call this-makefile";
			public const string parent_makefile = "call parent-makefile";
			public const string grand_parent_makefile = "call grand-parent-makefile";
		}


		public struct  AppVars {
			public static  readonly Var APP_PROJECT_PATH = new Var ("APP_PROJECT_PATH");
			
			public static  readonly Var APP_MODULES = new Var ("APP_MODULES");
			public static  readonly Var APP_OPTIM = new Var ("APP_OPTIM");
			public static  readonly Var APP_BUILD_SCRIPT = new Var ("APP_BUILD_SCRIPT");
			public static  readonly Var APP_ABI = new Var ("APP_ABI");
			public static  readonly Var APP_PLATFORM = new Var ("APP_PLATFORM");
			public static  readonly Var APP_STL = new Var ("APP_STL");
			public static  readonly Var APP_GNUSTL_FORCE_CPP_FEATURES = new Var ("APP_GNUSTL_FORCE_CPP_FEATURES");
			public static  readonly Var APP_SHORT_COMMANDS = new Var ("APP_SHORT_COMMANDS");
			public static  readonly Var NDK_TOOLCHAIN_VERSION = new Var ("NDK_TOOLCHAIN_VERSION");
			public static  readonly Var 	APP_PIE = new Var ("APP_PIE");
			public static  readonly Var 	APP_THIN_ARCHIVE = new Var ("APP_THIN_ARCHIVE");
			
			
			//used in Android.mk
			public static  readonly Var 	APP_CFLAGS = new Var ("APP_CFLAGS");
			public static  readonly Var 	APP_CPPFLAGS = new Var ("APP_CPPFLAGS");
			public static  readonly Var APP_LDFLAGS = new Var ("APP_LDFLAGS");
		}

		//include
		private static string inc (string x){
			return "include $(" + x + ")";
		}

		//get value
		private static string val(string x){
			return "$(" + x + ")";
		}



		public struct APP_ABI {
			public const string armeabi_v7a = "armeabi-v7a";
			public const string arm64_v8a = "arm64-v8a";
			public const string x86 = "x86";
			public const string x86_64 = "x86_64";
			public const string mips = "mips64";
			public const string armeabi = "all";
		}



		public struct APP_STL{
			public const string port_static = "stlport_static";
			public const string port_shared = "stlport_shared";
			public const string system = "system";
			
		}

		public struct APP_OPTIM{
			public const string release = "release";
			public const string debug = "debug";

			
		}

		//Templates 

		public static class Basic {
			public static string androidMK (string name, string[] src_files){
				string file = "";
				string n = '\n'.ToString();

				file += Vars.LOCAL_PATH.eq (Macros.my_dir) + n;
				file += Vars.LOCAL_MODULE.eq (name) + n;
				file += Vars.LOCAL_SRC_FILES.eq(src_files) + n;

				file += inc (IncVars.BUILD_SHARED_LIBRARY)+ n;

				return file;
			}

			public static string applicationMK (){
				string file = "";
				string n = '\n'.ToString();
				
				file += AppVars.APP_OPTIM.eq (APP_OPTIM.debug) + n;
				file += AppVars.APP_ABI.eq (APP_ABI.armeabi) + n;
				file += AppVars.APP_PLATFORM.eq ("android-9")+ n;
				
				file += inc (IncVars.BUILD_SHARED_LIBRARY)+ n;
				
				return file;
			}


		}



	}




	public class AndroidStudio  {



		private struct Module {
			string main;
		}

		private List <Module> modules ;



		static public string traverse (string projectPath){
			string main;

			try
			{
				

				
				foreach (string d in Directory.GetDirectories(projectPath))
				{
//					if(d.Equals("main"){
//						return d;
//					}
//					traverse(d);

				}


				
			}
			catch (System.Exception excpt)
			{
				Debug.LogError(excpt.Message);
			}
			return "";
		}

	}
	
}