﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;
using AndroidNative;
public class SDKactions  {


	static class Coms {
		public const  string javac = "javac";

		
	}

	static  class Parms
	{
		public static readonly Parm classpath = new Parm ("-classpath", true, ";",":");
		public static readonly Parm dir = new Parm ("-d", true);
		public static readonly Parm source = new Parm ("-source", true);
		public static readonly Parm target = new Parm ("-target", true);
		public static readonly Parm bootclasspath = new Parm ("-bootclasspath", true, ";",":");
		public static readonly Parm extdirs = new Parm ("-extdirs", true, ";",":");
		public static readonly Parm verbose = new Parm ("-verbose", true);
		public static readonly Parm sourcepath = new Parm ("-sourcepath", true, ";",":");




	}

	static class Vals 
	{
		public const string ANDROID_JAR = "/platforms/android-10/android.jar";
	}

	public static void compile(string src){
		string dir = src;
		string file = Path.Combine (Lib.JDKpath, Coms.javac);

		createDir (src, "classes");
		//string arg = args.classpath +  NDKcompile.Global.nativePath + " com/web_tomorrow/CPTest1.java";

		string arg = Javac.arguments(
			new string [] {Parms.classpath.value (src) , Parms.dir.value(src +"/classes")}
		, new string[] { "com/web_tomorrow/CPTest2.java"}) ;


		OS.run (file, arg,dir);

	}

	public static void generateJAR (string src){
		string file = Path.Combine (Lib.JDKpath, Jar.jar);
		string dir = src;

		string arg = Jar.arguments ("cf", "SUCESS.jar", src);

		OS.run (file, arg,dir);
	}


	private static void createDir(string dir, string subdir){


		string pathString = System.IO.Path.Combine(dir, subdir);
		if (Directory.Exists(pathString)){

			Directory.Delete(pathString,true);

		}
		System.IO.Directory.CreateDirectory(pathString);

	}


}
