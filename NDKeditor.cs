﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using AndroidNative;
//using System.Diagnostics;

public class NDKeditor : EditorWindow {
	static string unityPath = UnityEditor.EditorApplication.applicationPath;
	static string projectPath = Application.dataPath;
	// Use this for initialization


	[MenuItem ("TOOLS/settings")]
	
	public static void  ShowWindow () {

		NDKeditor window = (NDKeditor)EditorWindow.GetWindow(typeof(NDKeditor));
		window.Show ();

	}


	void OnEnable() {

	}

	 void OnGUI () {
		// The actual window code goes here
		//EditorUtility.GetAssetPreview(source);
		

		gui ();



		
		
	}

	void gui () {

		EditorGUILayout.BeginHorizontal ();
				
		GUILayout.Label ("NDK path ",GUILayout.MaxWidth(60));
		GUILayout.TextArea ( Lib.NDKpath);
		if (GUILayout.Button ("Browse", GUILayout.MaxWidth (60))) {


				string temp = EditorUtility.OpenFolderPanel ("Choose NDK directory", "", "");
			if(!temp.Equals(""))
				Lib.NDKpath = temp;
		}
			
		
		EditorGUILayout.EndHorizontal ();


		EditorGUILayout.BeginHorizontal ();
		
		GUILayout.Label ("SDK path ",GUILayout.MaxWidth(60));
		GUILayout.TextArea ( Lib.SDKpath);
		if (GUILayout.Button ("Browse", GUILayout.MaxWidth (60))) {
			
			
			string temp = EditorUtility.OpenFolderPanel ("Choose NDK directory", "", "");
			if(!temp.Equals(""))
				Lib.SDKpath = temp;
		}

		
		EditorGUILayout.EndHorizontal ();


		EditorGUILayout.BeginHorizontal ();
		
		GUILayout.Label ("JDK path ",GUILayout.MaxWidth(60));
		GUILayout.TextArea ( Lib.JDKpath);
		if (GUILayout.Button ("Browse", GUILayout.MaxWidth (60))) {
			
			
			string temp = EditorUtility.OpenFolderPanel ("Choose NDK directory", "", "");
			if(!temp.Equals(""))
				Lib.JDKpath = temp;
		}
		

		EditorGUILayout.EndHorizontal ();
		


		
	}


//	[MenuItem ("Window/LevelBuilder")]
//	static void Generate () {
//		//path =  Application.dataPath;
//
//#if UNITY_EDITOR_OSX
//
//			unityPath += "/../../";
//		
//#elif UNITY_EDITOR_WIN
//			path += "\..\";
//#endif
//		Debug.Log(projectPath);
//		string fileName = "tsdsdst.cpp";
//		
//		string pathString = System.IO.Path.Combine(projectPath, fileName);
//
//
//	Debug.Log("Path to my file: {0}\n");
//
//        // Check that the file doesn't already exist. If it doesn't exist, create 
//        // the file and write integers 0 - 99 to it. 
//        // DANGER: System.IO.File.Create will overwrite the file if it already exists. 
//        // This could happen even with random file names, although it is unlikely. 
//        if (!System.IO.File.Exists(pathString))
//        {
//            using (System.IO.FileStream fs = System.IO.File.Create(pathString))
//            {
//                for (byte i = 0; i < 100; i++)
//                {
//                    fs.WriteByte(i);
//                }
//            }
//        }
//        else
//        {
//            Debug.Log("File \"{0}\" already exists.");
//		return;
//	}
//	
//	// Read and display the data from your file. 
//
//	
//	// Keep the console window open in debug mode.
//	Debug.Log("Press any key to exit.");
//	System.Console.ReadKey();
//
//
//
//	}




}




