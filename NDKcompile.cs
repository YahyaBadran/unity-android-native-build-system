﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using AndroidNative;
public class NDKcompile : EditorWindow {


	static public string NDKpath = Lib.NDKpath ;


	[System.Serializable]
	public static  class Global {
		

		//static public string nativePath = System.IO.Path.Combine( Application.dataPath , "Plugins/Android/src/" ) ;
		static public string nativePath = "/Users/a/Desktop/DEL/src/classes";
		
	}


	[MenuItem ("TOOLS/Compile")]
	
	public static void  ShowWindow () {
		
		NDKcompile window = (NDKcompile)EditorWindow.GetWindow(typeof(NDKcompile));
		window.Show ();
		
	}
	// Update is called once per frame
	void OnGUI () {

		EditorGUILayout.BeginHorizontal ();
		
		GUILayout.Label ("Project ",GUILayout.MaxWidth(60));
		GUILayout.TextArea ( Global.nativePath);
		if (GUILayout.Button ("Browse", GUILayout.MaxWidth (60))) {
			string temp = EditorUtility.OpenFolderPanel ("Choose SRC directory", "", "");

			if(!temp.Equals(""))
				Global.nativePath = temp;
		
		}
		EditorGUILayout.EndHorizontal ();


		if (GUILayout.Button ("compile NDK")) {
			NDKactions.compile ( Global.nativePath);
		}

		if (GUILayout.Button ("compile SDK")) {
			SDKactions.compile ( Global.nativePath);
		}

		if (GUILayout.Button ("Generate JAR")) {
			SDKactions.generateJAR ( Global.nativePath);
		}

		if (GUILayout.Button ("Generate XML")) {
			XMLparser.xml();
		}
		if(GUILayout.Button("Write")){
			string x = MK.Basic.androidMK("hell",new string[] {"hee/hee","doo/daa"});
			string y = MK.Basic.applicationMK();

			System.IO.File.WriteAllText(@"/Users/a/Desktop/Android.mk", x);
			System.IO.File.WriteAllText(@"/Users/a/Desktop/Application.mk", y);
		}
	}
}
